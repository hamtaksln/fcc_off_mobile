/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
} from 'react-native';

import WebView from 'react-native-webview';

const CodeEditor = () => {
  const editorUri = 'file:///android_asset/editor.html';
  const indexUri = 'file:///android_asset/index.html';

  return (
    <WebView
      source={{uri: editorUri}}
      //   source={{uri: indexUri, baseUrl: './'}}
      //   source={{uri: "https://fcc-offline.herokuapp.com/learn/javascript-algorithms-and-data-structures/"}}
      javaScriptEnabled={true}
      originWhitelist={['*']}
      allowFileAccess={true}
      style={
        {
          // width: '100%',
          // marginTop: 20,
          // fontSize: '50px',
        }
      }
    />
  );
};

const App = () => {
  const isDarkMode = useColorScheme() === 'dark';

  return (
    <View style={styles.container}>
      <CodeEditor />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    fontSize: '4rem',
  },
});

export default App;
